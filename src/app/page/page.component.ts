// app/page/page.component.ts
import { Component, OnInit } from '@angular/core';
import { ContentService } from '../shared/services/content.service';
import { ActivatedRoute } from '@angular/router';

export interface IPage {
  title: string;
  subtitle: string;
  content: string;
  image: string;
}

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  // page?: IPage ;
  page = {
    title: 'Home',
    subtitle: 'Welcome Home!',
    content: 'Some home content.',
    image: 'assets/code-wallpaper-6.jpg'
  };

  constructor(private route: ActivatedRoute,
              private contentService: ContentService) { }

  ngOnInit() {
    // const pageData = this.route.snapshot.data['page'];
    // this.page = this.contentService.pages[pageData];
  }
}
