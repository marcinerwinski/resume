import { Injectable } from '@angular/core';

@Injectable()
export class ContentService {
  pages: any = {
    'home': {title: 'Home', subtitle: 'Welcome Home!', content: 'Some home content.', image: 'assets/code-wallpaper-6.png'},
    'about': {title: 'About', subtitle: 'About Us', content: 'Some content about us.', image: 'assets/code-wallpaper-6.png'},
    'contact': {title: 'Contact', subtitle: 'Contact Us', content: 'How to contact us.', image: 'assets/code-wallpaper-6.png'}
  };
}
